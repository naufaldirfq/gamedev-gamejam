extends KinematicBody2D

export (int) var speed = 500
export (int) var GRAVITY = 40
export (int) var jump_speed = -900

const UP = Vector2(0,-1)

var velocity = Vector2(speed, 0)

var is_dead = false

func _physics_process(delta):
	if is_dead == false:
		
		velocity.x += 0.2
		
		if velocity.x != 0 and velocity.y == 0:
			$AnimatedSprite.play("Run")
				
		if is_on_floor() and Input.is_action_just_pressed('ui_up'):
			velocity.y = jump_speed
			$AnimatedSprite.play("Jump")
			
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, UP)
		
		if is_on_wall():
			dead()
		if get_slide_count() > 0:
			for i in range(get_slide_count()):
				if "Cactus" in get_slide_collision(i).collider.name:
					dead()
		
func dead():
	is_dead = true
	velocity = Vector2(0,0)
	$AnimatedSprite.play("Dead")
	$CollisionShape2D.disabled = true
	$Timer.start()
	
func _on_Timer_timeout():
	get_tree().reload_current_scene()
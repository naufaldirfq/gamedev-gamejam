extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	$ColorRect/MarginContainer/CenterContainer/HBoxContainer/TextureButton.grab_focus()
	
func _physics_process(delta):
	if $ColorRect/MarginContainer/CenterContainer/HBoxContainer/TextureButton.is_hovered() == true:
		$ColorRect/MarginContainer/CenterContainer/HBoxContainer/TextureButton.grab_focus()
	if $ColorRect/MarginContainer/CenterContainer/HBoxContainer/TextureButton2.is_hovered() == true:
		$ColorRect/MarginContainer/CenterContainer/HBoxContainer/TextureButton2.grab_focus()

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		$ColorRect/MarginContainer/CenterContainer/HBoxContainer/TextureButton.grab_focus()
		pause()
		print(get_tree().get_current_scene().get_name())
		visible = not visible


func _on_TextureButton_pressed():
	pause()
	visible = not visible


func _on_TextureButton2_pressed():
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
	
func pause():
	get_tree().paused = not get_tree().paused
